<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Place;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class CreatePlaceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('CP')
            // ->add('place_user', EntityType::class, array(
            //     'class'=>Place::class,
            //     'choice_label'=>'User',
            //     'multiple'=>true,
            //     'expanded'=>true,
            // ))
            // On ajoute le champ "place_photo" dans le formulaire
            // Il n'est pas lié à la base de données (mapped à false)
            // ->add('place_photo', FileType::class,[
            //     'label' => false,
            //     'multiple' => true,
            //     'mapped' => false,
            //     'required' => false
            // ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Place::class,
        ]);
    }
}
