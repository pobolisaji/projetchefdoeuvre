<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Photo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class EditPhotoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // ->add('title')
            ->add('name')
            // ->add('description')
            // ->add('photo_user', EntityType::class,[
            //     'class' => User::class,
            //     'choice_label' => 'username',
            // ])
            // ->add('date')
            // ->add('adress')
            // ->add('place')
            // ->add('photo_user', FileType::class,[
            //     'label' => false,
            //     'multiple' => false,
            //     'mapped' => false,
            //     'required' => true
            // ])
            // On ajoute le champ "images" dans le formulaire
            // Il n'est pas lié à la base de données (mapped à false)
            // ->add('images', FileType::class,[
            //     'label' => false,
            //     'multiple' => true,
            //     'mapped' => false,
            //     'required' => false
            // ])
            // ->add('place_photo', FileType::class,[
            //     'label' => false,
            //     'multiple' => true,
            //     'mapped' => false,
            //     'required' => false
            // ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Photo::class,
        ]);
    }
}
