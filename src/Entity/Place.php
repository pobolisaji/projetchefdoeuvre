<?php

namespace App\Entity;

use App\Repository\PlaceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PlaceRepository::class)
 */
class Place
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $CP;

    /**
     * @ORM\OneToMany(targetEntity=Photo::class, mappedBy="place", cascade={"persist"})
     */
    private $place_photo;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="places")
     */
    private $place_user;

    public function __construct()
    {
        $this->place_photo = new ArrayCollection();
        $this->place_user = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCP(): ?int
    {
        return $this->CP;
    }

    public function setCP(int $CP): self
    {
        $this->CP = $CP;

        return $this;
    }

    /**
     * @return Collection|Photo[]
     */
    public function getPlacePhoto(): Collection
    {
        return $this->place_photo;
    }

    public function addPlacePhoto(Photo $placePhoto): self
    {
        if (!$this->place_photo->contains($placePhoto)) {
            $this->place_photo[] = $placePhoto;
            $placePhoto->setPlace($this);
        }

        return $this;
    }

    public function removePlacePhoto(Photo $placePhoto): self
    {
        if ($this->place_photo->removeElement($placePhoto)) {
            // set the owning side to null (unless already changed)
            if ($placePhoto->getPlace() === $this) {
                $placePhoto->setPlace(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getPlaceUser(): Collection
    {
        return $this->place_user;
    }

    public function addPlaceUser(User $placeUser): self
    {
        if (!$this->place_user->contains($placeUser)) {
            $this->place_user[] = $placeUser;
        }

        return $this;
    }

    public function removePlaceUser(User $placeUser): self
    {
        $this->place_user->removeElement($placeUser);

        return $this;
    }
}
