<?php

namespace App\Controller;

use App\Entity\Photo;
use App\Entity\Place;
use App\Form\PlaceType;
use App\Form\ImgPlaceType;
use App\Form\CreatePlaceType;
use App\Repository\PlaceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/profile/place')]
class PlaceController extends AbstractController
{
    #[Route('/', name: 'place_index', methods: ['GET'])]
    public function index(PlaceRepository $placeRepository): Response
    {
    
        return $this->render('place/index.html.twig', [
            'places' => $placeRepository->findBy(array(), array('name' => 'ASC')),
        ]);
    }

    #[Route('/new', name: 'place_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $place = new Place();
        $form = $this->createForm(CreatePlaceType::class, $place);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) { //plusieurs ajouts possibles
            // // On récupère les images transmises
            // $place_photo = $form->get('place_photo')->getData();
            
            // // On boucle sur les images
            // foreach($place_photo as $placePhoto){
            //     // On génère un nouveau nom de fichier
            //     $fichier = md5(uniqid()).'.'.$placePhoto->guessExtension();
            
            //     // On copie le fichier dans le dossier uploads
            //     $placePhoto->move(
            //         $this->getParameter('images_directory'),
            //         $fichier
            //     );
            
            //     // On crée l'image dans la base de données
            //     $img = new Photo();
            //     $img->setTitle($fichier);
            //     $place->addPlacePhoto($img);
            // }   

            // $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($place);
            $entityManager->flush();

            return $this->redirectToRoute('place_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('place/new.html.twig', [
            'place' => $place,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'place_show', methods: ['GET', 'POST'])]
    public function show(Request $request, Place $place, EntityManagerInterface $entityManager): Response
    {
        
        $form = $this->createForm(ImgPlaceType::class, $place);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // On récupère les images transmises
            $place_photo = $form->get('place_photo')->getData();
            
            // On boucle sur les images
            foreach($place_photo as $placePhoto){
                // On génère un nouveau nom de fichier
                $fichier = md5(uniqid()).'.'.$placePhoto->guessExtension();
            
                // On copie le fichier dans le dossier uploads
                $placePhoto->move(
                    $this->getParameter('images_directory'),
                    $fichier
                );
            
                // On crée l'image dans la base de données
                $img = new Photo();
                $img->setTitle($fichier);
                $place->addPlacePhoto($img);
            }   

            // $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($place);
            $entityManager->flush();

            return $this->redirectToRoute('place_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('place/show.html.twig', [
            'place' => $place,
            'form' => $form,
        ]);
    
    }

    #[Route('/{id}/edit', name: 'place_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Place $place, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(PlaceType::class, $place);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // On récupère les images transmises
            $place_photo = $form->get('place_photo')->getData();
            
            // On boucle sur les images
            foreach($place_photo as $placePhoto){
                // On génère un nouveau nom de fichier
                $fichier = md5(uniqid()).'.'.$placePhoto->guessExtension();
            
                // On copie le fichier dans le dossier uploads
                $placePhoto->move(
                    $this->getParameter('images_directory'),
                    $fichier
                );
            
                // On crée l'image dans la base de données
                $img = new Photo();
                $img->setTitle($fichier);
                $place->addPlacePhoto($img);
            }   

            // $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($place);
            $entityManager->flush();

            return $this->redirectToRoute('place_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('place/edit.html.twig', [
            'place' => $place,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'place_delete', methods: ['GET','POST'])]
    public function delete(Request $request, Place $place, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$place->getId(), $request->request->get('_token'))) {
            $entityManager->remove($place);
            $entityManager->flush();
        }

        return $this->redirectToRoute('place_index', [], Response::HTTP_SEE_OTHER);
    }
}
