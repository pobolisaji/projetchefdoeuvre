<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Place;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AccountController extends AbstractController
{
    #[Route('/profile/account', name: 'account')]
    public function index(): Response
    {
        return $this->render('account/index.html.twig', [
            'controller_name' => 'AccountController',
        ]);
    }

    #[Route('/profile/account/bibliotheque', name: 'bibliotheque')]
    public function bibliotheque(): Response
    {
        return $this->render('account/bibliotheque.html.twig', [
            'controller_name' => 'AccountController',
        ]);
    }

    #[Route('/profile/account/parameters', name: 'parameters')]
    public function parameters(): Response
    {
        return $this->render('account/parameters.html.twig', [
            'controller_name' => 'AccountController',
        ]);
    }

    // #[Route('/profile/account/parameters/delete', name: 'user_delete', methods: ['POST'])]
    // public function delete(Request $request, User $user, EntityManagerInterface $entityManager): Response
    // {
    //     if ($this->isCsrfTokenValid('delete', $request->request->get('_token'))) {
    //         $entityManager->remove($user);
    //         $entityManager->flush();
    //     }

    //     return $this->redirectToRoute('accueil', [], Response::HTTP_SEE_OTHER);
    // }
}
