<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Photo;
use App\Entity\Place;
use App\Form\PhotoType;
use App\Form\EditPhotoType;
use App\Repository\PhotoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/profile/photo')]
class PhotoController extends AbstractController
{
    #[Route('/', name: 'photo_index', methods: ['GET'])]
    public function index(PhotoRepository $photoRepository): Response
    {
    
        return $this->render('photo/index.html.twig', [
            // 'photos' => $photoRepository->findBy(['photo_user'=> $this->getUser()->getId()]), //récupère le repository, regarde si l'id du user connecté correspond à l'id de la photo
            'photos' => $this->getUser()->getPhotos(), // variable 'photos' => récupère toute l'entité User et dans cette entité récupère les photos qui y sont reliées
        ]);
    }

    
    #[Route('/new/{id}', name: 'photo_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager, Place $place): Response
    {
        $photo = new Photo();
        $form = $this->createForm(PhotoType::class, $photo);
        $form->handleRequest($request);
        $idUser = $this->getUser(); // pour récupérer le User connecté et l'associer à la photo chargée
     
        // dump($photo);

        if ($form->isSubmitted() && $form->isValid()) {
            // On récupère les images transmises
            $place_photo = $form->get('place_photo')->getData();
            
            // On boucle sur les images
            foreach($place_photo as $placePhoto){
                // On génère un nouveau nom de fichier
                $fichier = md5(uniqid()).'.'.$placePhoto->guessExtension();
            
                // On copie le fichier dans le dossier uploads
                $placePhoto->move(
                    $this->getParameter('images_directory'),
                    $fichier
                );
            
                // // On crée l'image dans la base de données
                // $img = new Photo();
                $photo->setTitle($fichier);
                $photo->setPhotoUser($idUser);// pour récupérer le User connecté et l'associer à la photo chargée
                $place->addPlacePhoto($photo);

            }
            $photo->setPlace($place);
            $entityManager->persist($photo);
            $entityManager->flush();

            return $this->redirectToRoute('place_show', ['id' => $place->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('photo/new.html.twig', [
            'photo' => $photo,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'photo_show', methods: ['GET'])]
    public function show(Photo $photo): Response
    {
        return $this->render('photo/show.html.twig', [
            'photo' => $photo,
        ]);
    }

    #[Route('/{id}/edit', name: 'photo_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Photo $photo, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(EditPhotoType::class, $photo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('photo_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('photo/edit.html.twig', [
            'photo' => $photo,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'photo_delete', methods: ['POST'])]
    public function delete(Request $request, Photo $photo, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$photo->getId(), $request->request->get('_token'))) {
            $entityManager->remove($photo);
            $entityManager->flush();
        }

        return $this->redirectToRoute('photo_index', [], Response::HTTP_SEE_OTHER);
    }
}
